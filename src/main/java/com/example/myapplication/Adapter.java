package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Adapter extends ArrayAdapter<String> {
    AnimalList animal_list;
        public Adapter(Context context, String[] countries) {
            super(context, 0, countries);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            String animalname =(String) getItem(position);
            if (view == null) {
                view = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
            }
            final Animal animal = animal_list.getAnimal(animalname);
            ImageView image = (ImageView) view.findViewById(R.id.imageC);
            TextView animalName = (TextView) view.findViewById(R.id.countryN);
            int id = view.getResources().getIdentifier("com.example.myapplication:drawable/" +animal.getImgFile() , null, null);
            image.setImageResource(id);
            animalName.setText(animalname);
            return view;
        }


}
