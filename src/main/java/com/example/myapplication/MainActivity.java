package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private String[] animal;
    AnimalList  animal_list;
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        animal=animal_list.getNameArray();
        listView=(ListView) findViewById(R.id.listPrincipal);


        final Adapter adapter = new Adapter(this,animal);
        listView.setAdapter(adapter);



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {

                Intent myIntent = new Intent(v.getContext(),Edit_activity.class);
                String animalChoice=(String)(listView.getItemAtPosition(position));
                myIntent.putExtra("animalChoice",animalChoice);
                startActivity(myIntent);


            }
        });



    }

}

