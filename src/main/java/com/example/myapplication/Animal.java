package com.example.myapplication;

/*public class Country {

    private String mCapital;
    private String mImgFile;
    private String mLanguage;
    private String mCurrency;
    private int mPopulation;
    private int mArea;

    public Country(String mCapital, String mImgFile, String mLanguage, String mCurrency, int mPopulation, int mArea) {
        this.mCapital = mCapital;
        this.mImgFile = mImgFile;
        this.mLanguage = mLanguage;
        this.mCurrency = mCurrency;
        this.mPopulation = mPopulation;
        this.mArea = mArea;
    }

    public String getmCapital() {
        return mCapital;
    }

    public void setmCapital(String mCapital) {
        this.mCapital = mCapital;
    }

    public String getmImgFile() {
        return mImgFile;
    }

    public void setmImgFile(String mImgFile) {
        this.mImgFile = mImgFile;
    }

    public String getmLanguage() {
        return mLanguage;
    }

    public void setmLanguage(String mLanguage) {
        this.mLanguage = mLanguage;
    }

    public String getmCurrency() {
        return mCurrency;
    }

    public void setmCurrency(String mCurrency) {
        this.mCurrency = mCurrency;
    }

    public int getmPopulation() {
        return mPopulation;
    }

    public void setmPopulation(int mPopulation) {
        this.mPopulation = mPopulation;
    }

    public int getmArea() {
        return mArea;
    }

    public void setmArea(int mArea) {
        this.mArea = mArea;
    }

}
*/

public class  Animal {

    private int hightestLifespan; // years
    private String imgFile;
    private int gestationPeriod; // days
    private float birthWeight; // kg
    private int adultWeight; // kg
    private String conservationStatus;

    public Animal(int hightestLifespan, String imgFile, int gestationPeriod, float birthWeight, int adultWeight, String conservationStatus) {
        this.hightestLifespan = hightestLifespan;
        this.imgFile = imgFile;
        this.gestationPeriod = gestationPeriod;
        this.birthWeight = birthWeight;
        this.adultWeight = adultWeight;
        this.conservationStatus = conservationStatus;
    }


    public int getHightestLifespan() {
        return hightestLifespan;
    }

    public String getStrHightestLifespan() {
        return Integer.toString(hightestLifespan)+" années";
    }

    public void setHightestLifespan(int hightestLifespan) {
        this.hightestLifespan = hightestLifespan;
    }

    public String getImgFile() {
        return imgFile; }

    public void setImgFile(String imgFile) {
        this.imgFile = imgFile;
    }

    public int getGestationPeriod() {
        return gestationPeriod;
    }

    public String getStrGestationPeriod() {
        return Integer.toString(gestationPeriod)+" jours";
    }

    public void setGestationPeriod(int gestationPeriod) {
        this.gestationPeriod = gestationPeriod;
    }

    public float getBirthWeight() {
        return birthWeight;
    }

    public String getStrBirthWpréoccupation_mineureeight() {
        return Float.toString(birthWeight)+" kg";
    }

    public void setBirthWeight(int birthWeight) {
        this.birthWeight = birthWeight;
    }

    public int getAdultWeight() {
        return adultWeight;
    }

    public String getStrAdultWeight() {
        return Integer.toString(adultWeight)+" kg";
    }

    public void setAdultWeight(int adultWeight) {
        this.adultWeight = adultWeight;
    }

    public String getConservationStatus() {
        return conservationStatus;
    }

    public void setConservationStatus(String conservationStatus) {
        this.conservationStatus = conservationStatus;
    }

}
