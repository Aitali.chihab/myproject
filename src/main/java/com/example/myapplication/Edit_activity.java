package com.example.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Edit_activity extends AppCompatActivity {
    AnimalList listAnimal;

    String animalSelectedName;
    Animal animalSelected;
    ImageView image;
    TextView animalName;
    EditText AhightestLifespan;;
    EditText AgestationPeriod;
    EditText AbirthWeight;
    EditText AadultWeight;
    EditText AconservationStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        listAnimal= new AnimalList();
        //Relie les variables et le View
        image = (ImageView) findViewById(R.id.imageView);
        animalName = (TextView) findViewById(R.id.animalName);
        AhightestLifespan = (EditText) findViewById(R.id.AhightestLifespan);
        AgestationPeriod= (EditText) findViewById(R.id. AgestationPeriod);
        AbirthWeight= (EditText) findViewById(R.id.AbirthWeight);
        AadultWeight = (EditText) findViewById(R.id.AadultWeight);
        AconservationStatus = (EditText) findViewById(R.id.AconservationStatus);

        //recuperation de l'animal selectionné
        Intent intent = getIntent();
        animalSelectedName = intent.getStringExtra("animalChoice");
        animalSelected  = listAnimal.getAnimal(animalSelectedName);

        //recuperation l'image de l'animal
        int id = getResources().getIdentifier("com.example.myapplication:drawable/" + animalSelected.getImgFile().toString(), null, null);
        image.setImageResource(id);

        //recuperation des information de l'animal
         animalName.setText(animalSelectedName.toString());
        AhightestLifespan.setText(animalSelected.getStrHightestLifespan().toString());
        AgestationPeriod.setText(animalSelected.getStrGestationPeriod().toString());
        AbirthWeight.setText(animalSelected.getStrBirthWpréoccupation_mineureeight().toString());
        AadultWeight.setText(String.valueOf(animalSelected.getStrAdultWeight()));
        AconservationStatus.setText(String.valueOf(animalSelected.getConservationStatus()));

        //le botton d'enregistrement
        Button save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    animalSelected.setHightestLifespan(Integer.valueOf(AhightestLifespan.getText().toString()));//INT
                    animalSelected.setGestationPeriod(Integer.valueOf(AgestationPeriod.getText().toString()));//INT
                    animalSelected.setBirthWeight(Integer.valueOf(AbirthWeight.getText().toString()));//FLOAT
                    animalSelected.setAdultWeight(Integer.valueOf(AadultWeight.getText().toString()));//INT
                    animalSelected.setConservationStatus(AconservationStatus.toString());//STRING
                    listAnimal.setAnimal(animalSelectedName, animalSelected);
                    Toast.makeText(getApplicationContext(), "la modification a bien été effectuée ", Toast.LENGTH_LONG).show();


            }
        });



}

}
